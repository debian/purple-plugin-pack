# Enhanced History

dependencies: pidgin
authors: Andrew Pangborn
introduced: 2.3.0

An enhanced version of the history plugin.  Grants ability to select the number of previous conversations to show instead of just one.

