# Show Offline

dependencies: purple
authors: Stu Tomlinson
introduced: 1.0beta1

Adds the option to show specific buddies in your buddy list when they are offline, even with "Show Offline Buddies" turned off.

