# Colorize

dependencies: libpurple
authors: Ike Gingerich
introduced: 2.4.0

Colorizes outgoing message text to a gradient of specified starting and ending RGB values.

