# BASH.org

dependencies: libpurple
authors: John Bailey
introduced: 1.0beta1

Generates links for quotes at bash.org or allows the user to specify a quote.  Provides the /bash command.

