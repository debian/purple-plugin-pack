# Google

dependencies: purple
authors: Gary Kramlich
introduced: 2.4.0

Writes the results of an "I'm feeling lucky" search to a conversation.

