# SNPP

dependencies: libpurple
authors: Don Seiler
introduced: 2.1.0

Allows libpurple to send messages over the Simple Network Paging Protocol (SNPP).

