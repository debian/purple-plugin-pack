# Switch Spell

dependencies: pidgin, gtkspell, aspell|enchant
authors: Sadrul Habib Chowdhury
introduced: 1.0beta7
notes: Works with both aspell- and enchant-enabled gtkspells, but may not intelligently detect one over the other during configure.

Switch Spell Checker Language.

