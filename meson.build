project('purple-plugin-pack', 'c',
  version : '2.8.0')

#######################################################################
# Our header
#######################################################################
add_project_arguments('-DHAVE_CONFIG_H', language : 'c')
conf = configuration_data()

conf.set_quoted('PP_VERSION', meson.project_version())

# I'm lazy and figured config.h is the best place for this ;)
conf.set_quoted('PP_WEBSITE', 'https://bitbucket.org/pidgin/purple-plugin-pack')

#######################################################################
# Good ol' gettext
#######################################################################

GETTEXT_PACKAGE = 'plugin_pack'
conf.set_quoted('GETTEXT_PACKAGE', GETTEXT_PACKAGE)

#######################################################################
# Look for the C compiler
#######################################################################
compiler = meson.get_compiler('c')

# this is used as the suffix below (for purple-3, pidgin-3, etc)
# and also as the -3 param to plugin_pack.py
if get_option('purple-version') == '2'
  PURPLE_VER = ''
  IS_PURPLE_TWO = true
else
  PURPLE_VER = '-' + get_option('purple-version')
  IS_PURPLE_TWO = false
endif

#######################################################################
# Check for purple
#######################################################################
PURPLE = dependency('purple@0@'.format(PURPLE_VER))

PURPLE_LIBDIR = PURPLE.get_pkgconfig_variable('plugindir')
PURPLE_DATADIR = PURPLE.get_pkgconfig_variable('datadir')

PP_LOCALEDIR = join_paths(get_option('prefix'), get_option('localedir'))
conf.set_quoted('PP_LOCALEDIR', PP_LOCALEDIR)

#######################################################################
# Check for pidgin
#######################################################################
PIDGIN = dependency('pidgin@0@'.format(PURPLE_VER), required : false)
conf.set('HAVE_PIDGIN', PIDGIN.found())

if PIDGIN.found()
	PIDGIN_LIBDIR = PIDGIN.get_pkgconfig_variable('plugindir')
	PIDGIN_DATADIR = PIDGIN.get_pkgconfig_variable('datadir')
	PIDGIN_PIXMAPSDIR = join_paths(PIDGIN_DATADIR, 'pixmaps/pidgin')
else
	PIDGIN_LIBDIR = ''
	PIDGIN_DATADIR = ''
	PIDGIN_PIXMAPSDIR = ''
endif

#######################################################################
# Check for finch
#######################################################################
FINCH = dependency('finch', required : false)
conf.set('HAVE_FINCH', FINCH.found())

if FINCH.found()
	FINCH_LIBDIR = FINCH.get_pkgconfig_variable('libdir')
	FINCH_DATADIR = FINCH.get_pkgconfig_variable('datadir')
else
	FINCH_LIBDIR = ''
	FINCH_DATADIR = ''
endif

#######################################################################
# check for gtk
#######################################################################
GLIB = dependency('glib-2.0', version : '>= 2.32.0', required : true)
GTK = dependency('gtk+-2.0', version : '>= 2.10.0', required : false)

#######################################################################
# check for gnt
#######################################################################
GNT = dependency('gnt', required : false)

#######################################################################
# check for pango
#######################################################################
PANGO = dependency('pango', required : false)

#######################################################################
# check for cairo
#######################################################################
CAIRO = dependency('cairo', required : false)

#######################################################################
# Check for libjson-glib
#######################################################################
JSON_GLIB = dependency('json-glib-1.0', required : false)

#######################################################################
# Check for ZLib
#######################################################################
ZLIB = dependency('zlib', required : false)

#######################################################################
# Check for switchspell
#######################################################################
GTKSPELL = dependency('gtkspell-2.0', version : '>= 2.0.2', required : false)

BUILD_SWITCH_SPELL = false

if GTKSPELL.found()
  # FIXME: This is a hack that should not exist.
  if run_command('pkg-config', '--static', '--libs', 'gtkspell-2.0').stdout().contains('enchant')
    ENCHANT = dependency('enchant-2', required : false)
    ASPELL = []
    BUILD_SWITCH_SPELL = ENCHANT.found()

    conf.set('HAVE_ENCHANT', ENCHANT.found())
  else
    ASPELL = dependency('aspell', required : false)
    ENCHANT = []
    BUILD_SWITCH_SPELL = ASPELL.found()
  endif
endif

#######################################################################
# Check for some basic headers
#######################################################################
if compiler.has_header('regex.h')
  conf.set('HAS_REGEX_H', true)
else
  error('regex.h cannot be found')
endif

#######################################################################
# Disable installation of translation files
#######################################################################
INSTALL_I18N = get_option('nls')
subdir('po')
conf.set('ENABLE_NLS', INSTALL_I18N)

#######################################################################
# Add all plugin directories:
#######################################################################
TYPES = get_option('types').split(',')
if TYPES.contains('all')
  TYPES = ['default', 'abusive', 'incomplete']
else
  foreach type : TYPES
    if not ['default', 'abusive', 'incomplete'].contains(type)
      error('"@0@" is not a supported plugin type'.format(type))
    endif
  endforeach
endif

PP_PURPLE_BUILD = []
PP_PIDGIN_BUILD = []
PP_FINCH_BUILD = []

subdir('album')
subdir('autoreply')
subdir('bash')
subdir('blistops')
subdir('capsnot')
subdir('colorize')
subdir('convbadger')
subdir('dewysiwygification')
subdir('dice')
subdir('difftopic')
subdir('eight_ball')
subdir('enhancedhist')
subdir('flip')
subdir('gRIM')
subdir('google')
subdir('groupmsg')
subdir('highlight')
subdir('icon-override')
subdir('ignore')
subdir('irc-more')
subdir('irchelper')
subdir('irssi')
subdir('lastseen')
subdir('listhandler')
subdir('listlog')
subdir('mystatusbox')
subdir('nicksaid')
subdir('ning')
subdir('okcupid')
subdir('oldlogger')
subdir('omegle')
subdir('plonkers')
subdir('schedule')
subdir('sepandtab')
subdir('showoffline')
subdir('simfix')
subdir('slashexec')
subdir('snpp')
subdir('splitter')
subdir('sslinfo')
subdir('switchspell')
subdir('timelog')
subdir('translate')
subdir('xmppprio')

#######################################################################
# Install the metainfo file
#######################################################################
install_data('purple-plugin-pack.metainfo.xml',
	install_dir : get_option('datadir') / 'metainfo')

#######################################################################
# Output!!
#######################################################################
configure_file(
  output : 'pp_config.h',
  configuration : conf)

rpm_conf = configuration_data()
rpm_conf.set('PACKAGE', meson.project_name())
rpm_conf.set('VERSION', meson.project_version())
configure_file(
  input : 'plugin_pack.spec.in',
  output : 'plugin_pack.spec',
  configuration : rpm_conf)

message('')
message('@0@ @1@ Configuration complete'.format(meson.project_name(), meson.project_version()))
message('')

message('Build purple plugins.............: ' + PURPLE.found().to_string())
if PURPLE.found()
  message('Installing purple plugins to.....: ' + PURPLE_LIBDIR)
  message('Installing purple plugin data to.: ' + PURPLE_DATADIR)
  message('Purple plugins to be built.......: ' + ' '.join(PP_PURPLE_BUILD))
endif
message('')

message('Build pidgin plugins.............: ' + PIDGIN.found().to_string())
if PIDGIN.found()
  message('Installing pidgin plugins to.....: ' + PIDGIN_LIBDIR)
  message('Installing pidgin plugin data to.: ' + PIDGIN_DATADIR)
  message('Pidgin plugins to be built.......: ' + ' '.join(PP_PIDGIN_BUILD))
endif
message('')

message('Build finch plugins..............: ' + FINCH.found().to_string())
if FINCH.found()
  message('Installing finch plugins to......: ' + FINCH_LIBDIR)
  message('Installing finch plugin data to..: ' + FINCH_DATADIR)
  message('Finch plugins to be built........: ' + ' '.join(PP_FINCH_BUILD))
endif
message('')

