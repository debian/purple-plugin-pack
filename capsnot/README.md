# Keyboard LED Notification

dependencies: libpurple
authors: Eion Robb
introduced: 2.7.0

A plugin for all libpurple clients that will flash the keyboard LED's when you receive a chat/IM message

