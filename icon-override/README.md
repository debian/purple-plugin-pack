# Protocol Icon Override

dependencies: pidgin
authors: Eion Robb
introduced: 2.7.0

Allows choosing a protocol icon on a per-account basis, allowing differentiation of multiple accounts of the same protocol.

