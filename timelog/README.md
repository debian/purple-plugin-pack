# TimeLog

dependencies: pidgin
authors: Jon Oberheide
introduced: 2.2.0

Allows the viewing of Pidgin logs within a specific time range.

