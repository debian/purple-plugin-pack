# Mystatusbox

dependencies: pidgin
authors: Sadrul Habib Chowdhury
introduced: 1.0beta1

You can show all the per-account statusboxes, hide all of them, or just show the ones that are in a different status from the global status.  For ease of use, you can bind keyboard shortcuts for the menu items.

