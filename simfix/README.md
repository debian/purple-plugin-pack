# SIM-fix

dependencies: libpurple
authors: Stu Tomlinson
introduced: 1.0beta1

Fixes messages received from broken SIM clients by stripping HTML from them. The buddy must be on your list and set as a SIM user.

