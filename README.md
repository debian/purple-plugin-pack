The Purple Plugin Pack was originally created by Gary Kramlich and Stu
Tomlinson as a way to distribute their ever growing lists of simple Pidgin
plugins. It has since grown from its origins of about 6 plugins to over 50.

Also, many more developers have continued to add to it, including John Bailey,
Peter Lawler, Sadrul Habib Chowdhury, Richard Laager, and Paul Aurich.  It has
also become a place to save plugins whose authors have since abandoned them.

As of June 2020, this project has moved to new hosting and the pieces can be
found at the corresponding links.

 * Repository: https://keep.imfreedom.org/pidgin/purple-plugin-pack
 * Issues: https://issues.imfreedom.org/issues/PLUGINPACK
 * Downloads: https://bintray.com/pidgin/releases/purple-plugin-pack

