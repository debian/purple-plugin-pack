# Group IM

dependencies: purple
authors: Stu Tomlinson
introduced: 1.0beta1

Adds the option to send an IM to every online buddy in a group.

