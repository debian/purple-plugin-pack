# Separate And Tab

dependencies: pidgin
authors: Gary Kramlich
introduced: 1.0beta1

Adds two new placement functions.  One separates IMs and groups chats in tabs, the other separates chats and groups IMs in tabs.

