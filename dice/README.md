# Dice

dependencies: purple
authors: Gary Kramlich
introduced: 1.0beta1

Adds a command (/dice) to roll an arbitrary number of dice with an arbitrary number of sides.  Now supports dice notation!  /help dice for details.

