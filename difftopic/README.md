# DiffTopic

dependencies: pidgin
authors: Sadrul Habib Chowdhury
introduced: 1.0beta4

Show the old topic when the topic in a chat room changes.

