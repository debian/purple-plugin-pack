# Omegle Protocol Plugin

dependencies: libpurple, json-glib
authors: Eion Robb
introduced: 2.7.0

Protocol plugin for the Omegle social network.

