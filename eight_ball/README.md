# Magic 8 Ball

dependencies: purple
authors: John Bailey
introduced: 1.0beta1

Provides Magic 8-ball like functionality with the /8ball command, as well as similar functionality for common Stargate words or phrases with the /sg-ball command.

