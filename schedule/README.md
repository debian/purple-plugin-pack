# Schedule

dependencies: pidgin
authors: Sadrul Habib Chowdhury
introduced: 1.0beta1
notes: Renamed from gaim-schedule to schedule after the Pidgin rename.

Schedule reminders at specified times.

