# Coin Flip

dependencies: purple
authors: Gary Kramlich
introduced: 1.0beta1

Adds a command (/flip) to flip a coin and outputs the result in the active conversation.

