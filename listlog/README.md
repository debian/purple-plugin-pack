# Chat User List Logging

dependencies: pidgin
authors: John Bailey
introduced: 2.4.0

When a chat is joined, this plugin will print the list of users in the chat in the conversation window, thus causing the userlist to be logged.

