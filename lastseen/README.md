# Last Seen

dependencies: pidgin
authors: Stu Tomlinson
introduced: 1.0beta1
notes: Partially superseded by functionality added in Pidgin 2.1.0.

Logs the time of a last received message, what they said, when they logged in, and when they logged out, for buddies on your buddy list.

