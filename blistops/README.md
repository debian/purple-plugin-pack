# Buddy List Options

dependencies: pidgin
authors: Gary Kramlich
introduced: 1.0beta1

Gives extended options to the buddy list.

