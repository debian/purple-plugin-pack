# OkCupid Protocol Plugin

dependencies: libpurplem, json-glib
authors: Eion Robb
introduced: 2.7.0

Protocol plugin for the OkCupid social networking site framework

