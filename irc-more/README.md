# IRC More

dependencies: libpurple
authors: Sadrul Habib Chowdhury,John Bailey
introduced: 2.2.0
notes: Support for /notice only when built with libpurple older than 2.4.0.

Adds additional IRC features, including a customizable quit message, a customizable CTCP VERSION reply, a rudimentary channel autojoin list and the /notice command for notices where libpurple does not support it.

