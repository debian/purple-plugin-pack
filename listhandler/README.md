# List Handler

dependencies: libpurple
authors: John Bailey
introduced: 1.0beta1

Provides numerous user-requested list-handling capabilities, such as importing and exporting of AIM .blt files and generic protocol-agnostic XML .blist files, as well as direct copying of buddies from one account to another.

