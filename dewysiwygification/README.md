# DeWYSIWYGification

dependencies: purple
authors: Tim Ringenbach
introduced: 2.2.0

Lets you type in HTML without it being escaped.  This will not work well for some protocols. Use "&lt;" for a literal "<".

