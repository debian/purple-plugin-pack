# XMPP Priority

dependencies: libpurple
authors: Paul Aurich
introduced: 2.6.0

Adds account options that allow users to specify the priorities used for available and away for XMPP accounts.

