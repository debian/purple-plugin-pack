# /exec

dependencies: libpurple
provides: slashexec
authors: Gary Kramlich, Peter Lawler, Daniel Atallah, John Bailey, Sadrul Habib Chowdhury
introduced: 1.0beta3

A plugin that adds the /exec command line interpreter like most UNIX/Linux IRC clients have.  Also included is the ability to execute commands with an exclamation point (!uptime, for instance)

