# SSL Info

dependencies: libpurple
authors: Gary Kramlich
introduced: 1.0beta1

Displays info about your currently loaded SSL plugin.

