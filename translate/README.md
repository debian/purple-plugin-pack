# Translate

dependencies: libpurple
authors: Eion Robb
introduced: 2.7.0

Provides automatic translation of messages using Google Translate (default) or Bing Translator.

